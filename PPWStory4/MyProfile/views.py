from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, "Home.html")

def misc(request):
    return render(request, "Misc.html")